import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  standalone: true,
  selector: 'ef-picker',
  imports: [
    CommonModule
  ],
  template: `
    <button class="capitalize" (click)="choose.emit(content)">
      {{ content }}
    </button>
  `
})
export class PickerComponent {
  @Input() content!: string;

  @Output() choose = new EventEmitter<string>();
}
