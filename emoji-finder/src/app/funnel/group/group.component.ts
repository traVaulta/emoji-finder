import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

import { Group } from '../../../domain/group';
import { EmojiComponent } from '../emoji/emoji.component';

@Component({
  standalone: true,
  selector: 'ef-group',
  imports: [
    CommonModule,
    EmojiComponent
  ],
  template: `
    <article class="w-full card">
      <article class="w-full flex justify-between items-center">
        <div><h4 class="capitalize">{{ group.name }}</h4></div>
        <div class="italic">{{ group.description }}</div>
      </article>
      <article class="flex items-baseline w-full">
        <ng-container *ngFor="let emoji of group.emojis">
          <ef-emoji [emoji]="emoji"></ef-emoji>
        </ng-container>
      </article>
    </article>
  `
})
export class GroupComponent {
  @Input() group!: Group;
}
