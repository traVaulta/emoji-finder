import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

import { Emoji } from '../../../domain/emoji';
import { toClipboard } from '../../../infra-ua/clipboard';

@Component({
  standalone: true,
  imports: [
    CommonModule
  ],
  selector: 'ef-emoji',
  template: `
    <button
      class="icon-only mr-1"
      [title]="emoji.name | titlecase"
      (click)="onClick()"
    >
      {{ emoji.content }}
    </button>
  `
})
export class EmojiComponent {
  @Input() emoji!: Emoji;

  onClick() {
    console.log(this.emoji.content);
    toClipboard(this.emoji.content)();
  }
}
