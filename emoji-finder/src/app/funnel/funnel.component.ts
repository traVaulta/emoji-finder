import { CommonModule } from '@angular/common';
import { Component, Inject, Input } from '@angular/core';

import { GroupComponent } from './group/group.component';
import { PickerComponent } from './picker/picker.component';
import { DATA, Raw } from '../../data';
import {
  chain,
  filterAll,
  filterGroups,
  or,
  preCond,
  present,
  rules
} from '../../domain/filter';
import { ALL_GROUPS, Group } from '../../domain/group';
import { fromPartial } from '../../domain/mapper';

@Component({
  standalone: true,
  selector: 'ef-funnel',
  imports: [
    CommonModule,
    GroupComponent,
    PickerComponent
  ],
  template: `
    <header class="flex gap justify-center items-center call-to-action">
      <ng-container *ngFor="let category of categories">
        <ef-picker [content]="category" (choose)="onFilter($event)">
        </ef-picker>
      </ng-container>
    </header>
    <ng-content></ng-content>
    <main class="flex flex-col justify-center items-center">
      <ng-container *ngIf="groups as all">
        <ng-container *ngFor="let group of all">
          <ef-group [group]="group"></ef-group>
        </ng-container>
      </ng-container>
    </main>
  `
})
export class FunnelComponent {
  @Input() query = '';

  readonly categories: string[] = [...ALL_GROUPS];
  readonly parsed: Group[] = fromPartial(this.data);

  filter = '';

  constructor(
    @Inject(DATA)
    private data: Raw
  ) { }

  get groups() {
    if (!this.filter.length && !this.query.length)
      return this.parsed;
    else {
      const pattern = new RegExp(this.query, 'i');

      const matchEmoji = preCond(
        or(
          rules.byNameE(pattern),
          rules.byDescE(pattern)
        ),
        present(this.query)
      );

      const matchGroup = filterGroups(preCond(
        rules.byName(this.filter),
        present(this.filter)
      ));
      const matchGroupByEmoji = filterAll(matchEmoji);

      const queryGroups = chain([
        matchGroup,
        matchGroupByEmoji
      ]);

      const groups = queryGroups(this.parsed);
      return !this.query.length ?
        groups :
        groups.map(g => (<Group>{
          ...g,
          emojis: g.emojis.filter(matchEmoji)
        }));
    }
  }

  onFilter(category: string) {
    if (this.filter !== category)
      this.filter = category;
    else
      this.filter = '';
  }
}
