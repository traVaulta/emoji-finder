import { Component } from '@angular/core';

@Component({
  selector: 'ef-root',
  template: `
    <ef-funnel [query]="query">
      <aside class="flex justify-center items-baseline gap">
        <label for="">Search:</label>
        <input type="text" [(ngModel)]="query">
      </aside>
    </ef-funnel>
  `
})
export class AppComponent {
  query = '';
}
