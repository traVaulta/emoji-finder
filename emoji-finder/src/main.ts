import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { DATA, preFetchData } from './data';

preFetchData().then(data =>
  platformBrowserDynamic([
    { provide: DATA, useValue: data }
  ]).bootstrapModule(AppModule)
    .catch(err => console.error(err))
);
