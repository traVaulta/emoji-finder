export const permission = <PermissionDescriptor>{
  name: <PermissionName>'clipboard-write'
};

export const toClipboard = (content: string) => async () => {
  const result = await navigator.permissions.query(permission);
  if (result.state === 'granted' || result.state === 'prompt') {
    await navigator.clipboard.writeText(content);
  }
};
