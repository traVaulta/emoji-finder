import { InjectionToken } from '@angular/core';

import { Group } from '../domain/group';

export type Raw = { [groupName: string]: Partial<Group> };

export const DATA = new InjectionToken<Raw>('data');

export const preFetchData = async (): Promise<Raw> => {
  const response = await fetch('/assets/emojis.json');
  return await response.json();
};
