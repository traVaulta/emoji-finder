import type { Emoji } from './emoji';
import type { Group } from './group';

const byName = (title: string) => ({ name }: Emoji|Group) => name === title;
const byNameE = (exp: RegExp) => ({ name }: Emoji|Group) => exp.test(name);
const byDescE = (exp: RegExp) => ({ description }: Emoji|Group) =>
  exp.test(description);

export const rules = {
  byName,
  byNameE,
  byDesc: (content: string) => byDescE(new RegExp(content)),
  byDescE
};

export const present = (value: string) => () => !!value.length;

export const preCond = <T=Emoji>(
  op: (e: T) => boolean,
  cond: () => boolean
) => (v: T) => !cond() || op(v);

export const or = <T=Emoji>(
  op1: (e: T) => boolean,
  op2: (e: T) => boolean
) => (v: T) => op1(v) || op2(v);

export const chain = (ops: ((groups: Group[]) => Group[])[]) =>
  (groups: Group[]) => ops.reduce((acc, op) => op(acc), groups);

export const filterEmojis = (criteriaFn: (e: Emoji) => boolean) =>
  ({ emojis }: Group) => emojis.some(criteriaFn);

export const filterGroups = (criteriaFn: (g: Group) => boolean) =>
  (groups: Group[]) => groups.filter(criteriaFn);

export const filterAll = (criteriaFn: (e: Emoji) => boolean) =>
  filterGroups(filterEmojis(criteriaFn));
