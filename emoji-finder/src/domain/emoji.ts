export interface Emoji {
  name: string;
  content: string;
  description: string;
}
