import type { Group } from './group';

// type OperationData = (p: string) => { [key: string]: Partial<Group> };
//
// export const fromFile = (path: string, operation: OperationData) => {
//   const result = operation(path);
//   console.log(result);
//   return Object.keys(result).reduce(
//     (acc, name) => [
//       ...acc,
//       {
//         ...result[name],
//         name
//       } as Group
//     ],
//     [] as Group[]
//   );
// };

export const fromPartial = (result: { [key: string]: Partial<Group> }) =>
  Object.keys(result).reduce(
    (acc, name) => [
      ...acc,
      {
        ...result[name],
        name
      } as Group
    ],
    [] as Group[]
  );
