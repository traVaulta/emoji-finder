import type { Emoji } from './emoji';

export interface Group {
  name: string;
  description: string;
  emojis: Emoji[];
}

export type Operation = (path: string) =>
  { [group: string]: Omit<Group, 'name'> };

export enum GroupType {
  faces = 'faces',
  gestures = 'gestures',
  utils = 'utils',
  nature = 'nature',
  food = 'food',
  sports = 'sports',
  flags = 'flags',
  signs = 'signs',
  misc = 'misc'
}

export const ALL_GROUPS = [
  GroupType.faces,
  GroupType.gestures,
  GroupType.utils,
  GroupType.nature,
  GroupType.food,
  GroupType.sports,
  GroupType.flags,
  GroupType.signs,
  GroupType.misc
];
