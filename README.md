# Windows 11 emoji finder app

Web app to help you find the right emoji in Windows 11.

## Motivation

Ever tried quickly adding an emoji in MS Teams, but not being able to scroll to groups of emojis to find it?
Or even if you found the one you like, you click it and the emoji dialog doesn't close?
Well this project should help you avoid all of that.

> Solution: a simple web app accessible from everywhere with simple filter to help you find the right emoji by
>  - previewing groups
>  - filtering by content, keyword or association &
>  - finally clicking on the emoji populates the clipboard with that content so you can quickly place the emoji into message input field of MS Teams.

## Resources

Emojis are versioned, so project will try to keep up, if achievable.

List of emojis:
- [emoji list](https://unicode.org/Public/emoji/14.0/emoji-sequences.txt)
- [test file](https://unicode.org/Public/emoji/14.0/emoji-test.txt)
- original [directory listing for v14](http://unicode.org/Public/emoji/14.0/)

## Unicode License

Unicode emoji list provided by Unicode, Inc. via [license](https://www.unicode.org/copyright.html).

## Author

- [Matija Čvrk](https://hr.linkedin.com/in/matija-%C4%8Dvrk-1388b3101)
